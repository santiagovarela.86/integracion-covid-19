# COVID TELEGRAM BOT API (CON SPRING BOOT)

Esta aplicación va a permitir al usuario interacturar con la api COVID API a travez de un bot de telegram.

## Tecnologías

Las tecnologías utilizadas en este proyecto son:

- Spring Boot como framework base
- Java 14 OpenJDK14.
- Gradle: gestor de paquetes.
- Lombok Annotations: Para generacion de Getters/Setters/Constructors simplifica el armado de DTOs & Entities https://projectlombok.org/ es recomendable instalar el plugin correspondiente para cada IDE (para visualizar correctamente)
- JUnit & Mockito: herramientas para test 
- Docker: gestor de containers para correr la aplicación y poder desacoplarnos del Sistema Operativo donde corren los distintos componentes de la aplicación.
- Docker Compose: Herramienta para poder correr aplicaciones compuestas por varios contenedores.

## Comandos del BOT

Esta sección va a estar dividida por entidad y su fin es brindar documentación extensiva sobre cada comando aceptado desde el bot de telegram.

### Como usar el BOT
1) Buscar el bot "TACS-G1-COVID" en telegram
2) Enviar comando **/start** para ver descripción del bot 
3) Autenticarse con el comando **/signup** *usuario* *contraseña*
4) Enviar comando **/menu** para mostrar el menu principal
5) Desloguearse con el comando **/signout**

## Operaciones que se pueden realizar con el bot (solo modo usuario)
- Consultar los últimos valores por lista y/o país.
- Consultar la tabla que compara los países de una lista (para los últimos X días)
- Agregar un pais a una lista.
- Revisar los paises de una lista (Nombre de los paises)

### Help
Para visualizar el help enviar el comando **/help**  