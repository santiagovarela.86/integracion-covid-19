package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.factories.CommandServiceFactory;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandResolverService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service("commandService")
public class DefaultCommandResolverService implements CommandResolverService {

    private final CommandServiceFactory commandServiceFactory;

    @Autowired
    public DefaultCommandResolverService(CommandServiceFactory commandServiceFactory) {
        this.commandServiceFactory = commandServiceFactory;
    }

    @Override
    public BotApiMethod<? extends Serializable> execute(Update update) {
        Command command = retrieveCommand(update);
        log.info("ChatId=" + retrieveChatId(update) + " - Command=" + command);
        return commandServiceFactory.create(command).process(update, retrieveArguments(update));
    }

    private Command retrieveCommand(Update update) {
        try {
            List<String> arguments = splitArguments(update);
            return Command.valueOf(arguments.get(0).replace("/", "").toUpperCase());
        } catch (Exception e) {
            log.error("Error parsing arguments");
        }
        return Command.HELP;
    }

    private List<String> retrieveArguments(Update update) {
        try {
            List<String> arguments = new ArrayList<>(splitArguments(update));
            arguments.remove(0);
            return arguments;
        } catch (Exception e) {
            log.error("Error parsing arguments");
        }
        return List.of();
    }

    protected List<String> splitArguments(Update update) {
        if (update.hasMessage()) {
            return Arrays.asList(update.getMessage().getText().split("\\s+"));
        }
        if (update.hasCallbackQuery()) {
            return Arrays.asList(update.getCallbackQuery().getData().split("\\s+"));
        }
        return List.of();
    }

    private Long retrieveChatId(Update update) {
        try {
            if (update.hasMessage()) {
                return update.getMessage().getChatId();
            } else if (update.hasCallbackQuery()) {
                return update.getCallbackQuery().getMessage().getChatId();
            }

        } catch (Exception e) {
            log.error("Error getChatId");
        }
        return null;
    }
}
