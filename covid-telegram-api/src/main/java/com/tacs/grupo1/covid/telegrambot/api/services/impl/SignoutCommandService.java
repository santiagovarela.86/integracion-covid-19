package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.dto.UserInformation;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.UserRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class SignoutCommandService implements CommandService {

    private final UserRepository userRepository;

    @Autowired
    public SignoutCommandService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {
        if (args.size() == 0){
            // Desasocia el userId de telegram del usuario de la api
            UserInformation user = userRepository.signoutUser(update.getMessage().getFrom().getId());

            if (user != null) {
                if (user.getUserName().equalsIgnoreCase("USER_DOESNT_EXIST")) {
                    return new SendMessage()
                            .setText("No existe la cuenta en el sistema, no se pudo desasociar el usuario")
                            .setChatId(update.getMessage().getChatId());
                } else {
                    if (user.getUserName().equalsIgnoreCase("WRONG_TELEGRAM_ID")) {
                        return new SendMessage()
                                .setText("Error en el ID de Telegram")
                                .setChatId(update.getMessage().getChatId());
                    } else {
                        if (user.getUserName().equalsIgnoreCase("USER_ALREADY_DEREGISTERED")) {
                            return new SendMessage()
                                    .setText("Esta cuenta ya está desregistrada")
                                    .setChatId(update.getMessage().getChatId());
                        } else {
                            return new SendMessage()
                                    .setText("Se desasoció la cuenta con éxito")
                                    .setChatId(update.getMessage().getChatId());
                        }
                    }
                }
            }else
            {
                return new SendMessage()
                        .setText("Error al intentar desasociar la cuenta, contacte al administrador")
                        .setChatId(update.getMessage().getChatId());
            }
        }else
        {
            return new SendMessage()
                    .setText("Ingrese el comando con la cantidad correcta de argumentos")
                    .setChatId(update.getMessage().getChatId());
        }
    }
}
