package com.tacs.grupo1.covid.telegrambot.api.bootstrap;

import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.LoginRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class JwtTokenInitializer {

    private String jwtToken = "";
    private LoginRepository loginRepository;

    @Autowired
    public JwtTokenInitializer(LoginRepository loginRepository){
        this.loginRepository = loginRepository;
    }

    @PostConstruct
    private void init() throws InterruptedException {
        while (!loginRepository.isApiHealthy()){
            log.info("Api not healthy yet, retrying in 10 seconds...");
            TimeUnit.SECONDS.sleep(10);
        }
        loginRepository.getToken();
    }
}
