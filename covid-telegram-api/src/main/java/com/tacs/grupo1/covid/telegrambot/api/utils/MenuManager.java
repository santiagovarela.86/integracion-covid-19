package com.tacs.grupo1.covid.telegrambot.api.utils;

import java.util.ArrayList;
import java.util.List;

public class MenuManager {

    //  Metodo modificado
    public List<MenuItem> getControlButtonsForPage(int page, int lastPage, String prevAction, String nextAction, String cancelAction, boolean hasCancel) {

        List<MenuItem> buttons = new ArrayList<>();
        if (page > 0) {
            buttons.add(new MenuItem("<<", prevAction));
        }
        if (hasCancel) {
            buttons.add(new MenuItem("Cancel", cancelAction));
        }
        if (page < lastPage) {
            buttons.add(new MenuItem(">>", nextAction));
        }
        return buttons;
    }

    public InlineKeyboardBuilder createMenuForPage(int page, List<MenuItem> pageButtons, List<MenuItem> controlButtons, int columnsCount) {
//        List<MenuItem> pageButtons = getPage(page);     // Obtiene una lista con los elementos de la página
//        List<MenuItem> controlButtons = getControlButtonsForPage(page, hasCancel); // Genera los botones de control como lista

        InlineKeyboardBuilder builder = InlineKeyboardBuilder.create();
        int col = 0;
        int num = 0;
        // ******************************** Agrega los botones para los datos de la página
        builder.row();
        for (MenuItem button : pageButtons) {
            builder.button(button.getName(), button.getAction());
            if (++col >= columnsCount) {
                col = 0;
                builder.endRow();
                if (num++ <= pageButtons.size()) {
                    builder.row();
                }
            }
        }
        builder.endRow();
        // ******************************** Termina de agregar los botones para los datos de la página
        // ******************************** Agrega los botones de control
        builder.row();
        for (MenuItem button : controlButtons) {
            builder.button(button.getName(), button.getAction());
        }
        builder.endRow();
        // ******************************** Termina de agregar los botones de control
        return builder;
    }

}