package com.tacs.grupo1.covid.telegrambot.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
public class ApplicationTelegramBot {

    public static void main(String[] args) {
        ApiContextInitializer.init();
        SpringApplication.run(ApplicationTelegramBot.class, args);
    }
}
