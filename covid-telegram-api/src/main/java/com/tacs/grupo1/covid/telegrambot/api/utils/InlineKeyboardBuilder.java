package com.tacs.grupo1.covid.telegrambot.api.utils;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

public class InlineKeyboardBuilder {

    private Long chatId;
    private String text;

    private List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
    private List<InlineKeyboardButton> row = null;

    private InlineKeyboardBuilder() {}

    public static InlineKeyboardBuilder create() {
        InlineKeyboardBuilder builder = new InlineKeyboardBuilder();
        return builder;
    }

    public static InlineKeyboardBuilder create(Long chatId) {
        InlineKeyboardBuilder builder = new InlineKeyboardBuilder();
        builder.setChatId(chatId);
        return builder;
    }

    public InlineKeyboardBuilder setText(String text) {
        this.text = text;
        return this;
    }

    public InlineKeyboardBuilder setChatId(Long chatId) {
        this.chatId = chatId;
        return this;
    }

    public InlineKeyboardBuilder row() {
        this.row = new ArrayList<>();
        return this;
    }

    public InlineKeyboardBuilder button(String text, String callbackData) {
        row.add(new InlineKeyboardButton().setText(text).setCallbackData(callbackData));
        return this;
    }

    public InlineKeyboardBuilder endRow() {
        this.keyboard.add(this.row);
        this.row = null;
        return this;
    }


    public SendMessage build() {
        SendMessage message = new SendMessage();

        message.setChatId(chatId);
        message.setText(text);

        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();

        keyboardMarkup.setKeyboard(keyboard);
        message.setReplyMarkup(keyboardMarkup);

        return message;
    }

    public InlineKeyboardMarkup buildInLineKeyboard() {

        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();

        keyboardMarkup.setKeyboard(keyboard);

        return keyboardMarkup;
    }


    public SendMessage buildSendMessage() {
        SendMessage message = new SendMessage();

        message.setChatId(chatId);
        message.setText(text);

        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();

        keyboardMarkup.setKeyboard(keyboard);
        message.setReplyMarkup(keyboardMarkup);

        return message;
    }

    public EditMessageText buildEditMessageText() {
        EditMessageText message = new EditMessageText();

        message.setChatId(chatId);
        message.setText(text);

        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();

        keyboardMarkup.setKeyboard(keyboard);
        message.setReplyMarkup(keyboardMarkup);

        return message;
    }

//  METODOS PARA LA PAGINACION *****************************************************************************************

    //  Metodo modificado
    public List<MenuItem> getControlButtonsForPage(int page, int lastPage, String prevAction, String nextAction, String cancelAction, boolean hasCancel) {

        List<MenuItem> buttons = new ArrayList<>();
        if (page > 0) {
            buttons.add(new MenuItem("<<", prevAction));
        }
        if (hasCancel) {
            buttons.add(new MenuItem("Back", cancelAction));
        }
        if (page < lastPage) {
            buttons.add(new MenuItem(">>", nextAction));
        }
        return buttons;
    }

    public InlineKeyboardBuilder createMenuForPage(int page, List<MenuItem> pageButtons, List<MenuItem> controlButtons, int columnsCount) {

        InlineKeyboardBuilder builder = InlineKeyboardBuilder.create();
        int col = 0;
        int num = 0;
        // ******************************** Agrega los botones para los datos de la página
        builder.row();
        for (MenuItem button : pageButtons) {
            builder.button(button.getName(), button.getAction());
            if (++col >= columnsCount) {
                col = 0;
                builder.endRow();
                if (num++ <= pageButtons.size()) {
                    builder.row();
                }
            }
        }
        builder.endRow();
        // ******************************** Termina de agregar los botones para los datos de la página
        // ******************************** Agrega los botones de control
        builder.row();
        for (MenuItem button : controlButtons) {
            builder.button(button.getName(), button.getAction());
        }
        builder.endRow();
        // ******************************** Termina de agregar los botones de control
        return builder;
    }
}