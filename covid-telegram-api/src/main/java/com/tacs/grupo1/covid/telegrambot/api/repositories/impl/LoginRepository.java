package com.tacs.grupo1.covid.telegrambot.api.repositories.impl;

import com.tacs.grupo1.covid.telegrambot.api.dto.TelegramUser;
import com.tacs.grupo1.covid.telegrambot.api.dto.UserInformation;
import com.tacs.grupo1.covid.telegrambot.api.exceptions.RestRepositoryException;
import com.tacs.grupo1.covid.telegrambot.api.repositories.AbstractRestRepository;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Repository
public class LoginRepository {

    @Value("${internalapiusername}")
    private String username;
    @Value("${internalapipassword}")
    private String password;
    @Value("${apiaddr}")
    private String apiaddr;
    private static String jwtToken;
    protected RestTemplate restClient;

    @Autowired
    public LoginRepository(RestTemplateBuilder restBuilder){
        this.restClient = restBuilder.build();
    }

    public String getToken() {
        if (jwtToken == null){
            try {
                JSONObject body = new JSONObject();
                body.put("user_name", username);
                body.put("password", password);
                String uri = getBaseUri() + "/login";
                ResponseEntity<String> response = post(uri, String.class, body.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response.getBody());
                    jwtToken = jsonObject.getString("token");
                    return jwtToken;
                }catch (JSONException e){
                    log.error(e.getMessage(), e);
                    throw new RestRepositoryException("JSON Formatting Exception");
                }
            } catch (RestClientException e) {
                log.error(e.getMessage(), e);
                throw new RestRepositoryException("Rest Client Exception");
            }
        }else
        {
            return jwtToken;
        }
    }

    public String refreshToken(){
        jwtToken = null;

        try {
            JSONObject body = new JSONObject();
            body.put("user_name", username);
            body.put("password", password);
            String uri = getBaseUri() + "/login";
            ResponseEntity<String> response = post(uri, String.class, body.toString());
            try {
                JSONObject jsonObject = new JSONObject(response.getBody());
                jwtToken = jsonObject.getString("token");
                return jwtToken;
            }catch (JSONException e){
                log.error(e.getMessage(), e);
                throw new RestRepositoryException("JSON Formatting Exception");
            }
        } catch (RestClientException e) {
            log.error(e.getMessage(), e);
            throw new RestRepositoryException("Rest Client Exception");
        }
    }

    //NEED MY OWN POST WITHOUT HEADERS
    protected <T> ResponseEntity<T> post(String uri, Class<T> responseType,  Object requestBody) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        ResponseEntity<T> response = restClient.exchange(
                uri,
                HttpMethod.POST,
                new HttpEntity<>(requestBody,headers),
                responseType);

        if (response.getStatusCode().is2xxSuccessful() || response.getStatusCode().is3xxRedirection()) {
            return response;
        }

        log.error("Error response", response);
        throw new RestRepositoryException("HTTP Status Code Not Successful while doing POST");
    }

    private String getBaseUri() {
        return this.apiaddr;
    }

    protected <T> ResponseEntity<T> get(String uri, Class<T> responseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<T> response = restClient.exchange(
                uri,
                HttpMethod.GET,
                entity,
                responseType);

        if (response.getStatusCode().is2xxSuccessful() || response.getStatusCode().is3xxRedirection()) {
            return response;
        }

        log.error("Error response", response);
        throw new RestRepositoryException("HTTP Status Code Not Successful while doing GET");
    }

    public Boolean isApiHealthy() {
        try {
            ResponseEntity<String> response = get(apiaddr + "/health", String.class);
            return true;
        } catch (RestClientException e) {
            return false;
        }
    }

}
