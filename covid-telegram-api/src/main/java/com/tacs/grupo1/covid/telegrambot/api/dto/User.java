package com.tacs.grupo1.covid.telegrambot.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class User {
    private Long id;
    @JsonProperty("user_name")
    private String userName;
    private String name;
    @JsonProperty("last_name")
    private String lastName;
    private String email;
    @JsonProperty("id_telegram")
    private Long idTelegram;
    private String result;
    private String message;
}
