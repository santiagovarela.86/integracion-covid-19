package com.tacs.grupo1.covid.telegrambot.api.bot;

import com.tacs.grupo1.covid.telegrambot.api.services.CommandResolverService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Slf4j
@Component
public class Telegram extends TelegramLongPollingBot {

    @Value("${BOT_NAME")
    private String botName;
    @Value("${BOT_TOKEN}")
    private String botToken;

    private final CommandResolverService commandResolverService;

    @Autowired
    public Telegram(CommandResolverService commandResolverService) {
        this.commandResolverService = commandResolverService;
    }

    @Override
    public void onUpdateReceived(Update update) {
        log.info("Reading message bot");
        try {
            execute(commandResolverService.execute(update));
        } catch (TelegramApiException e) {
            log.error("Error initializing bot", e);
        }
    }

    public String getBotUsername() {
        return this.botName;
    }

    public String getBotToken() {
        return this.botToken;
    }

}