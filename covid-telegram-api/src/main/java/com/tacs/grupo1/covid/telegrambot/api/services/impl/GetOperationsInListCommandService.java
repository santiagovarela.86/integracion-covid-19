package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.dto.UserInformation;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.utils.InlineKeyboardBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class GetOperationsInListCommandService implements CommandService {

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {

        if (update.hasCallbackQuery()) {
            return new EditMessageText()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText("¿Cuál operación desea realizar sobre la lista?")
                    .setParseMode(ParseMode.MARKDOWN)
                    .setReplyMarkup(InlineKeyboardBuilder.create()
                            .row()
                            .button("Ver paises", "/" + Command.GETCOUNTRIESINLIST.toString().toLowerCase() + " " + args.get(0))
                            .button("Agregar país", "/"+Command.GETCOUNTRIESINDEXFORLISTOP.toString().toLowerCase() + " " + args.get(0))
                            .endRow()
                            .row()
                            .button("<< Back", "/"+Command.GETOPLISTS.toString().toLowerCase())
                            .endRow()
                            .buildInLineKeyboard());
        }
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }
}
