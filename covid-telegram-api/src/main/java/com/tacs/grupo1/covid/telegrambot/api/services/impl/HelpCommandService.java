package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class HelpCommandService implements CommandService {
    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {

        String msj = "BOT TACS GRUPO 1 \n\n"
                + "Para poder empezar a operar debe asociar primero la cuenta de telegram\n"
                + "con un usuario dado de alta en la aplicación covid-api.\n"
                + "Esta operación se realiza solo una vez y puede desasociar la cuenta en cualquier momento.\n\n"
                + "- Asociar cuenta:\n"
                + "Enviar el mensaje /signup seguido del usuario y password registrados en la aplicacion covid-api\n"
                + "/signup @usuario @password\n\n"
                + "- Desasociar cuenta:\n"
                + "Enviar el mensaje /signout\n"
                + "/signout\n\n"
                + "Si ya registro una cuenta de usuario puede comenzar a operar con el comando\n"
                + "/menu\n\n"
                ;
        return new SendMessage()
                .setText(msj)
                .setChatId(update.getMessage().getChatId());
    }
}
