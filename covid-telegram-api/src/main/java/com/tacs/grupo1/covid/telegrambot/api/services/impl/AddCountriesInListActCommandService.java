package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.dto.CountryList;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.ListRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class AddCountriesInListActCommandService implements CommandService {

    private final ListRepository listRepository;

    @Autowired
    public AddCountriesInListActCommandService(ListRepository listRepository) {
        this.listRepository = listRepository;
    }


    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {
        if (update.hasCallbackQuery()) {

            listRepository.addCountry(Long.parseLong(args.get(0)),Long.parseLong(args.get(1)));
            /*
            Validar si se agrego o no el país
             */
            return new SendMessage()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setText("Se agregó el país a la lista satisfactoriamente");
        }
        else if (update.hasMessage()){
            return new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText("Comando no aceptado por mensaje!");
        }

        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }
}
