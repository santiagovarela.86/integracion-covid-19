package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.dto.Country;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.CountryRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.utils.InlineKeyboardBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.io.Serializable;
import java.util.List;

@Service
public class AddCountriesInListCommandService implements CommandService {

    private final CountryRepository countryRepository;

    @Autowired
    public AddCountriesInListCommandService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public BotApiMethod<Serializable> process(Update update, List<String> args) {
        if (update.hasCallbackQuery()) {
            /* Se recuperan los paises de la lista : args[0] y se agrega el cod del pais como segundo parametro en la accion de los botones*/
            return new EditMessageText()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText("Seleccionar país para agregar a la lista " + args.get(0))
                    .setParseMode(ParseMode.MARKDOWN)
                    .setReplyMarkup(generateKeyboardFromCountries(
                            countryRepository.getCountries(1),
                            "/"+ Command.ADDCOUNTRIESINLISTACT.toString().toLowerCase() + " " + args.get(0),
                            "/"+ Command.GETOPERATIONSINLIST.toString().toLowerCase() + " " + args.get(0)));
        }
        return new EditMessageText()
                .setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }

    private InlineKeyboardMarkup generateKeyboardFromCountries(List<Country> countries, String command, String backAction){
        InlineKeyboardBuilder keyboard = InlineKeyboardBuilder.create();

        boolean isEndRow=false;

        for (Country e: countries) {
            if(!isEndRow){
                keyboard
                        .row()
                        .button(e.getName() +"("+e.getIsoCountryCode()+")",
                                command + " " + e.getId());
            }
            else{
                keyboard
                        .button(e.getName() +"("+e.getIsoCountryCode()+")",
                                command + " " + e.getId())
                        .endRow();
            }
            isEndRow=!isEndRow;
        }

        if (isEndRow)
            keyboard.endRow();

        keyboard.row().button("<< Back",backAction).endRow();

        return keyboard.buildInLineKeyboard();
    }
}
