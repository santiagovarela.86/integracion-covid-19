package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.utils.InlineKeyboardBuilder;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class GetCountriesIndexForListOpCommandService implements CommandService {
    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {

        if (update.hasCallbackQuery()) {
            String idList = args.get(0);

            return new EditMessageText()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText("Índice de paises:")
                    .setParseMode(ParseMode.MARKDOWN)
                    .setReplyMarkup(InlineKeyboardBuilder.create()
                            .row()
                            .button("A", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " A" + " 0 " + idList)
                            .button("B", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " B" + " 0 " + idList)
                            .button("C", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " C" + " 0 " + idList)
                            .button("D", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " D" + " 0 " + idList)
                            .button("E", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " E" + " 0 " + idList)
                            .button("F", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " F" + " 0 " + idList)
                            .button("G", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " G" + " 0 " + idList)
                            .button("H", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " H" + " 0 " + idList)
                            .endRow()
                            .row()
                            .button("I", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " I" + " 0 " + idList)
                            .button("J", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " J" + " 0 " + idList)
                            .button("K", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " K" + " 0 " + idList)
                            .button("L", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " L" + " 0 " + idList)
                            .button("M", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " M" + " 0 " + idList)
                            .button("N", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " N" + " 0 " + idList)
                            .button("O", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " O" + " 0 " + idList)
                            .button("P", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " P" + " 0 " + idList)
                            .endRow()
                            .row()
                            .button("Q", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " Q" + " 0 " + idList)
                            .button("R", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " R" + " 0 " + idList)
                            .button("S", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " S" + " 0 " + idList)
                            .button("T", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " T" + " 0 " + idList)
                            .button("U", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " U" + " 0 " + idList)
                            .button("V", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " V" + " 0 " + idList)
                            .button("W", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " W" + " 0 " + idList)
                            .button("X", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " X" + " 0 " + idList)
                            .endRow()
                            .row()
                            .button("Y", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " Y" + " 0 " + idList)
                            .button("Z", "/"+Command.DOCOUNTRIESSEARCHFORLISTOP.toString().toLowerCase() + " Z" + " 0 " + idList)
                            .endRow()
                            .row()
                            .button("<< Back", "/"+ Command.GETOPERATIONSINLIST.toString().toLowerCase() + " " + idList)
                            .endRow()
                            .buildInLineKeyboard());
        }
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }
}
