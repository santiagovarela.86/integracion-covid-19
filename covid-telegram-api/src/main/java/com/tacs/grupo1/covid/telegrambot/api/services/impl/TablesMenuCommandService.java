package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.dto.statistics.CountryListPlot;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.StatisticsRepository;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.UserRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class TablesMenuCommandService implements CommandService {
    private final UserRepository userRepository;

    @Autowired
    public TablesMenuCommandService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {

        if (update.hasCallbackQuery()){
            String msj = "";

//            UserInformation user =userRepository.validateUser(update.getMessage().getFrom().getId());

            msj = "Para visualizar las tablas use los siguientes comandos "
            +"según el criterio de comparación.\n"
            +"Reemplazar X por la cantidad de días (últimos días) que quiere comparar\n\n"
            +"/Confirmados X\n\n"
            +"/Recuperados X\n\n"
            +"/Muertos X\n";

            // El userId ya esta registrado
            return new SendMessage()
                    .setText(msj)
                    .setChatId(update.getCallbackQuery().getMessage().getChatId());

        }
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado!");
    }
}

