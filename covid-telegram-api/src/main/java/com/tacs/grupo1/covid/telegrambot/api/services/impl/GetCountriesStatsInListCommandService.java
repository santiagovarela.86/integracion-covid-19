package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.dto.statistics.CountryListStatistics;
import com.tacs.grupo1.covid.telegrambot.api.dto.statistics.CountryStatistics;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.StatisticsRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class GetCountriesStatsInListCommandService implements CommandService {

    private final StatisticsRepository statisticsRepository;

    @Autowired
    public GetCountriesStatsInListCommandService(StatisticsRepository statisticsRepository) {
        this.statisticsRepository = statisticsRepository;
    }

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {
        if (update.hasCallbackQuery()) {
            /* Se busca las estadisticas de los paises de la lista: args[0] */

            CountryListStatistics countryListStatistics = statisticsRepository
                    .getCountryListStatistics(Long.parseLong(args.get(0)), 1);

            return new SendMessage()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setText(buildMessageResponse(countryListStatistics));
//                    .setText("Informacion de los paises en la lista " + args.get(0));
        }
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }

    public String buildMessageResponse(CountryListStatistics res){

        String msj="Lista: " + res.getName() + "\n\n";

        for ( CountryStatistics cs: res.getCountryStatisticsList()) {

            msj+= cs.getCountry().getName() + "\n"
            + "confirmed: " + cs.getStatistics().getConfirmed() + "\n"
            + "recovered: " + cs.getStatistics().getRecovered() + "\n"
            + "deaths: " + cs.getStatistics().getDeaths() + "\n\n";
        }

        return msj;
    }


}
