package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import org.telegram.telegrambots.meta.api.objects.Update;

public class DefaultUserInformationService {
    // @Todo preparar el servicio que retorne un DTO TelegramUser que contenga toda la informacion
    public void getContact(Update update) {
        System.out.println("User Id: " + update.getMessage().getFrom().getId());
        System.out.println("Chat Id: " + update.getMessage().getChatId());
        System.out.println("User Name: " + update.getMessage().getFrom().getUserName());
        System.out.println("First Name: " + update.getMessage().getFrom().getFirstName());
        System.out.println("Last Name: " + update.getMessage().getFrom().getLastName());
        System.out.println("Command: " + update.getMessage().getText());
        System.out.println("+------------------------------------+");
    }
}
