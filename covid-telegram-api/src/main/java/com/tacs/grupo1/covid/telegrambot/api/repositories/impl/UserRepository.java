package com.tacs.grupo1.covid.telegrambot.api.repositories.impl;

import com.tacs.grupo1.covid.telegrambot.api.dto.TelegramUser;
import com.tacs.grupo1.covid.telegrambot.api.dto.UserInformation;
import com.tacs.grupo1.covid.telegrambot.api.exceptions.RestRepositoryException;
import com.tacs.grupo1.covid.telegrambot.api.repositories.AbstractRestRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientException;

@Slf4j
@Repository
public class UserRepository extends AbstractRestRepository {

    private LoginRepository loginRepository;
    @Value("${apiaddr}")
    private String apiaddr;

    @Autowired
    public UserRepository(RestTemplateBuilder restBuilder, LoginRepository loginRepository) {
        super(restBuilder, loginRepository);
    }

    public UserInformation validateUser(long telegramId) {
        try {
            String uri = builValidateUserUrl();
            ResponseEntity<UserInformation> response = getv2(uri, UserInformation.class, telegramId);
            return response.getBody();
        } catch (RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while validating user");
        }

    }

    public UserInformation signupUser(TelegramUser telegramUser) {
        try {
            String uri = builSignupUserUrl(telegramUser);
            ResponseEntity<UserInformation> response = post(uri,UserInformation.class, telegramUser);
            return response.getBody();
        } catch (RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while signing up user");
        }

    }

    public UserInformation signoutUser(long userId) {
        try {
            String uri = builSignoutUserUrl(userId);
            ResponseEntity<UserInformation> response = post(uri, UserInformation.class, userId);
            return response.getBody();
        } catch (RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while signing out user");
        }

    }

    private String builValidateUserUrl() {
        return getUriBuilder(getBaseUri() + "/user-telegram").toUriString();
    }

    private String builSignupUserUrl(TelegramUser user) {
        return getUriBuilder(getBaseUri() + "/user-telegram").toUriString();
    }

    private String builSignoutUserUrl(long userId) {
        return getUriBuilder(getBaseUri() + "/user-telegram/" + userId)
                .queryParam("telegram_user_id", userId).toUriString();
    }

    //SEE HOW WE CAN PASS THIS AS A PARAMETER DURING BUILD...
    private String getBaseUri() {
        return apiaddr;
    }

}
