package com.tacs.grupo1.covid.telegrambot.api.domain;

import lombok.Getter;

@Getter
public enum Command {
    START,              // Mensaje de inicio del bot
    MENU,               // MenuCommandService: Carga el menu principal
    GETREQOPTION,       //  Busca las opciones sobre las que hacer consultas
    GETOPLISTS,         //  Busca las operaciones que se pueden hacer sobre listas
    GETREQCOUNTRIES,    //  Genera el menu para la consulta por pais
    GETCOUNTRYSTATS,    //  Consulta los ultimos datos de un pais determinado
    GETREQLISTS,        //  Genera el menu para listar mis listas (para ver estadisticas de los paises)
    GETCOUNTRIESSTATSINLIST,    // Consulta las ultimas estadisticas de los paises de una lista
    GETOPERATIONSINLIST,    // Muestras las operaciones que se pueden  hacer sobre una lista
    GETCOUNTRIESINLIST,     // Muesta los paises de una lista
    ADDCOUNTRIESINLIST,     // Agrega una pais a una lista
    ADDCOUNTRIESINLISTACT,  // Realiza la accion de agregar un pais a una lista
    HELP,
    SIGNUP,             //  Registra idTelegram con cuanta de usuario
    SIGNOUT,            //  Desconecta idTelegram con cuanta de usuario
    TABLESMENU,            //  Menu para ver las tablas de comparación
    CONFIRMADOS,            // Tabla de comparación por confirmados
    RECUPERADOS,            // Tabla de comparación por recuperados
    MUERTOS,            // Tabla de comparación por muertos
    GENSTATISTICSTABLE,     // Obtiene la info y la envia al usuario
    GETCOUNTRIESINDEXFORCONSULT,    // Indice de paises para consultar estadisticas
    DOCOUNTRIESSEARCHFORCONSULT,    // Busca paises por página para consultar estadisticas
    GETCOUNTRIESINDEXFORLISTOP,     // Indice de paises para agregar país a una lista
    DOCOUNTRIESSEARCHFORLISTOP,     // Busca paises por página para agregar país a una lista
    GETIMG;            //  PRUEBA: DEVUELVE UNA IMAGEN
}
