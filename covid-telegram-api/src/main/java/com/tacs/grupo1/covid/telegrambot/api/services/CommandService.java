package com.tacs.grupo1.covid.telegrambot.api.services;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@FunctionalInterface
public interface CommandService {
    BotApiMethod<? extends Serializable> process(Update update, List<String> args);
}
