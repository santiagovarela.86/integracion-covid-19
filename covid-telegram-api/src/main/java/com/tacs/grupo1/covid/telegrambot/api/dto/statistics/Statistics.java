package com.tacs.grupo1.covid.telegrambot.api.dto.statistics;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Statistics {
    protected Long confirmed;
    protected Long deaths;
    protected Long recovered;
}
