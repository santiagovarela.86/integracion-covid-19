package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.dto.CountriesPage;
import com.tacs.grupo1.covid.telegrambot.api.dto.Country;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.CountryRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.utils.InlineKeyboardBuilder;
import com.tacs.grupo1.covid.telegrambot.api.utils.MenuItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Service
public class DoCountriesSearchForConsultCommandService implements CommandService {

    private final CountryRepository countryRepository;

    @Autowired
    public DoCountriesSearchForConsultCommandService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {

        if (update.hasCallbackQuery()) {

            if(args.isEmpty()){
                return new SendMessage()
                        .setChatId(update.getCallbackQuery().getMessage().getChatId())
                        .setText("Faltan parámetros!");
            }

            return new EditMessageText()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText("Seleccionar país:")
                    .setParseMode(ParseMode.MARKDOWN)
                    .setReplyMarkup(generateKeyboardFromCountriesPage(
                            countryRepository.getCountriesPage(args.get(0),Integer.parseInt(args.get(1))),
                            "/"+Command.GETCOUNTRYSTATS.toString().toLowerCase(),
                            "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " " + args.get(0) + " ",
                            "/"+Command.DOCOUNTRIESSEARCHFORCONSULT.toString().toLowerCase() + " " + args.get(0) + " ",
                            "/"+Command.GETCOUNTRIESINDEXFORCONSULT.toString().toLowerCase()));

        }
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }

    private InlineKeyboardMarkup generateKeyboardFromCountriesPage(CountriesPage countriesPage, String commandAction,
             String prevAction, String nextAction, String cancelAction){

        int page = countriesPage.getPageNumber();//Empieza en cero
        int lastPage = countriesPage.getTotalPages();
        List<Country> countriesList = countriesPage.getCountryList();

        InlineKeyboardBuilder keyboard = InlineKeyboardBuilder.create();

        List<MenuItem> controlButtons = keyboard.getControlButtonsForPage(page,(lastPage-1),
                prevAction + " "+ (page-1),
                nextAction + " "+ (page+1),
                cancelAction, true);

        List<MenuItem> elementsButtons = new ArrayList<MenuItem>();
        for (Country e: countriesList) {
            elementsButtons.add(new MenuItem(e.getName(),commandAction+ " " + e.getId()));
        }

        InlineKeyboardBuilder inlineKeyboard = keyboard.createMenuForPage(page, elementsButtons, controlButtons, 2);

        return inlineKeyboard.buildInLineKeyboard();
    }
}
