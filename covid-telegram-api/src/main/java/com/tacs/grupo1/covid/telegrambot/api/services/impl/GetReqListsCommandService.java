package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.domain.Command;
import com.tacs.grupo1.covid.telegrambot.api.dto.Country;
import com.tacs.grupo1.covid.telegrambot.api.dto.CountryList;
import com.tacs.grupo1.covid.telegrambot.api.dto.TelegramUser;
import com.tacs.grupo1.covid.telegrambot.api.dto.UserInformation;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.ListRepository;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.UserRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import com.tacs.grupo1.covid.telegrambot.api.utils.InlineKeyboardBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.io.Serializable;
import java.util.List;

@Service
public class GetReqListsCommandService implements CommandService {

    private final ListRepository listRepository;
    private final UserRepository userRepository;

    @Autowired
    public GetReqListsCommandService(ListRepository listRepository, UserRepository userRepository) {
        this.listRepository = listRepository;
        this.userRepository = userRepository;
    }
    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {
        if (update.hasCallbackQuery()) {
            UserInformation userInformation = userRepository.validateUser(update.getCallbackQuery().getMessage().getChatId());
            if(userInformation.getUserName()==null || userInformation.getUserName()=="")
                return new SendMessage().setChatId(update.getMessage().getChatId()).setText("Cliente telegram no registrado!");

            return new EditMessageText()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText("Consultar ultimos valores de los paises en la lista...")
                    .setParseMode(ParseMode.MARKDOWN)
                    .setReplyMarkup(generateKeyboardFromCountriesList(
                        listRepository.getCountriesListByTelegramId(userInformation.getTelegramId()),
                        "/" + Command.GETCOUNTRIESSTATSINLIST.toString().toLowerCase(),
                        "/"+ Command.GETREQOPTION.toString().toLowerCase()));

        }
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Comando no aceptado por mensaje!");
    }

    private InlineKeyboardMarkup generateKeyboardFromCountriesList(List<CountryList> lists, String command, String backAction){
        InlineKeyboardBuilder keyboard = InlineKeyboardBuilder.create();
        boolean isEndRow=false;

        for (CountryList e: lists) {
            if(!isEndRow){
                keyboard
                        .row()
                        .button(e.getName(),
                                command + " " + e.getId());
            }
            else{
                keyboard
                        .button(e.getName(),
                                command + " " + e.getId())
                        .endRow();
            }
            isEndRow=!isEndRow;
        }

        if (isEndRow)
            keyboard.endRow();

        keyboard.row().button("<< Back",backAction).endRow();

        return keyboard.buildInLineKeyboard();
    }
}
