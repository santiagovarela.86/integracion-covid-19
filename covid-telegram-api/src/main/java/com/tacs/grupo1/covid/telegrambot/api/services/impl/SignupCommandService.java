package com.tacs.grupo1.covid.telegrambot.api.services.impl;

import com.tacs.grupo1.covid.telegrambot.api.dto.TelegramUser;
import com.tacs.grupo1.covid.telegrambot.api.dto.UserInformation;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.UserRepository;
import com.tacs.grupo1.covid.telegrambot.api.services.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.List;

@Service
public class SignupCommandService implements CommandService {

    private final UserRepository userRepository;

    @Autowired
    public SignupCommandService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public BotApiMethod<? extends Serializable> process(Update update, List<String> args) {

        if (args.size() == 2){
            UserInformation user = userRepository.signupUser(new TelegramUser()
                    .setTelegramId(Long.parseLong("" + update.getMessage().getFrom().getId()))
                    .setUserName(args.get(0))
                    .setPassword(args.get(1)));

            if (user.getUserName() != null) {
                if (user.getUserName().equalsIgnoreCase("USER_DOESNT_EXIST")) {
                    return new SendMessage()
                            .setText("No existe la cuenta en el sistema, no se pudo asociar el usuario")
                            .setChatId(update.getMessage().getChatId());
                } else {
                    if (user.getUserName().equalsIgnoreCase("BAD_CREDENTIALS")) {
                        return new SendMessage()
                                .setText("Error en las credenciales, intente nuevamente")
                                .setChatId(update.getMessage().getChatId());
                    } else {
                        if (user.getUserName().equalsIgnoreCase("USER_ALREADY_REGISTERED")) {
                            return new SendMessage()
                                    .setText("Esta cuenta ya está registrada")
                                    .setChatId(update.getMessage().getChatId());
                        } else {
                            return new SendMessage()
                                    .setText("Se registró la cuenta de telegram al usuario " + user.getUserName() +".\n\n"
                                    +"Envie el mensaje /menu para comenzar a operar")
                                    .setChatId(update.getMessage().getChatId());
                        }
                    }
                }
            } else {
                return new SendMessage()
                        .setText("Error al intentar asociar la cuenta, contacte al administrador")
                        .setChatId(update.getMessage().getChatId());
            }
        }else
        {
            return new SendMessage()
                    .setText("Ingrese el comando con la cantidad correcta de argumentos")
                    .setChatId(update.getMessage().getChatId());
        }
    }
}
