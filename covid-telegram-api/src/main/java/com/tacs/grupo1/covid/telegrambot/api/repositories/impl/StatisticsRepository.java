package com.tacs.grupo1.covid.telegrambot.api.repositories.impl;

import com.tacs.grupo1.covid.telegrambot.api.dto.statistics.CountryListPlot;
import com.tacs.grupo1.covid.telegrambot.api.dto.statistics.CountryListStatistics;
import com.tacs.grupo1.covid.telegrambot.api.dto.statistics.CountryStatistics;
import com.tacs.grupo1.covid.telegrambot.api.exceptions.RestRepositoryException;
import com.tacs.grupo1.covid.telegrambot.api.repositories.AbstractRestRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientException;

@Slf4j
@Repository
public class StatisticsRepository extends AbstractRestRepository {

    private LoginRepository loginRepository;
    @Value("${apiaddr}")
    private String apiaddr;

    @Autowired
    public StatisticsRepository(RestTemplateBuilder restBuilder, LoginRepository loginRepository)
    {
        super(restBuilder, loginRepository);
    }

    public CountryStatistics getCountryStatistics(long id, long userId) {
        try {
            String uri = buildGetCountryStaticsUrl(id, userId);
            ResponseEntity<CountryStatistics> response = get(uri, CountryStatistics.class);
            return response.getBody();
        } catch (RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while getting Country Statistics");
        }
    }

    public CountryListStatistics getCountryListStatistics(long id, long userId) {
        try {
            String uri = buildGetCountryListStaticsUrl(id, userId);
            ResponseEntity<CountryListStatistics> response = get(uri, CountryListStatistics.class);
            return response.getBody();
        } catch (RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while getting Country List Statistics");
        }
    }

    public CountryListPlot getCountryListStatisticsLastDays(long id, long lastDays) {
        try {
            String uri = buildGetCountryListStaticsLastDaysUrl(id, lastDays);
            ResponseEntity<CountryListPlot> response = get(uri, CountryListPlot.class);
            return response.getBody();
        } catch (RestClientException e) {
            log.error("Error response", e);
            throw new RestRepositoryException("API Error while getting Country List Statistics Last Days");
        }
    }

    private String buildGetCountryListStaticsLastDaysUrl(long id, long lastDays) {
        return getUriBuilder(getBaseUri() + "/country-lists/" + id + "/plot")
                .queryParam("d", lastDays).toUriString();
    }

    private String buildGetCountryListStaticsUrl(long id, long userId) {
        return getUriBuilder(getBaseUri() + "/country-lists/" + id + "/stats")
                .queryParam("telegram_user_id", userId).toUriString();
    }

    private String buildGetCountryStaticsUrl(long id, long userId) {
        return getUriBuilder(getBaseUri() + "/countries/" + id + "/stats")
                .queryParam("telegram_user_id", userId).toUriString();
    }

    //SEE HOW WE CAN PASS THIS AS A PARAMETER DURING BUILD...
    private String getBaseUri() {
        return apiaddr;
    }

}
