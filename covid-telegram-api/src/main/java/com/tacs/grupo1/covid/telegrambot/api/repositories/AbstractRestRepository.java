package com.tacs.grupo1.covid.telegrambot.api.repositories;

import com.tacs.grupo1.covid.telegrambot.api.exceptions.RestRepositoryException;
import com.tacs.grupo1.covid.telegrambot.api.repositories.impl.LoginRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
public abstract class AbstractRestRepository {

    protected RestTemplate restClient;
    private LoginRepository loginRepository;

    @Autowired
    public AbstractRestRepository(
            RestTemplateBuilder restBuilder,
            LoginRepository loginRepository
    ) {
        this.restClient = restBuilder.build();
        this.loginRepository = loginRepository;
    }

    protected <T> ResponseEntity<T> get(String uri, Class<T> responseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", "Bearer " + loginRepository.getToken());
        HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<T> response = null;

        try {
            response = restClient.exchange(
                    uri,
                    HttpMethod.GET,
                    entity,
                    responseType);
        } catch (HttpClientErrorException.Unauthorized e) {
            //single retry
            String newToken = loginRepository.refreshToken();
            headers.set("Authorization", "Bearer " + newToken);

            response = restClient.exchange(
                    uri,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    responseType);

            return response;
        }

        if (response.getStatusCode().is2xxSuccessful() || response.getStatusCode().is3xxRedirection()) {
            return response;
        }

        log.error("Error response", response);
        throw new RestRepositoryException("HTTP Status Code Not Successful while doing GET");
    }

    protected <T> ResponseEntity<T> getv2(String uri, Class<T> responseType, long telegramId) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", "Bearer " + loginRepository.getToken());
        headers.set("Authorization2", "Telegram " + telegramId);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<T> response = null;

        try {
            response = restClient.exchange(
                    uri,
                    HttpMethod.GET,
                    entity,
                    responseType);
        } catch (HttpClientErrorException.Unauthorized e) {
            //single retry
            String newToken = loginRepository.refreshToken();
            headers.set("Authorization", "Bearer " + newToken);

            response = restClient.exchange(
                    uri,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    responseType);

            return response;
        }

        if (response.getStatusCode().is2xxSuccessful() || response.getStatusCode().is3xxRedirection()) {
            return response;
        }

        log.error("Error response", response);
        throw new RestRepositoryException("HTTP Status Code Not Successful while doing GETv2");
    }

    protected <T> ResponseEntity<T> post(String uri, Class<T> responseType, Object requestBody) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", "Bearer " + loginRepository.getToken());
        HttpEntity<?> entity = new HttpEntity<>(requestBody, headers);
        ResponseEntity<T> response = null;

        try {
            response = restClient.exchange(
                    uri,
                    HttpMethod.POST,
                    entity,
                    responseType);
        } catch (HttpClientErrorException.Unauthorized e) {
            //single retry
            String newToken = loginRepository.refreshToken();
            headers.set("Authorization", "Bearer " + newToken);

            response = restClient.exchange(
                    uri,
                    HttpMethod.POST,
                    new HttpEntity<>(requestBody, headers),
                    responseType)
            ;

            return response;
        }

        if (response.getStatusCode().is2xxSuccessful() || response.getStatusCode().is3xxRedirection()) {
            return response;
        }

        log.error("Error response", response);
        throw new RestRepositoryException("HTTP Status Code Not Successful while doing POST");
    }

    /*
    protected <T> ResponseEntity<T> delete(String uri, Class<T> responseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", "Bearer " + loginRepository.getToken());
        HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<T> response = restClient.exchange(
                uri,
                HttpMethod.DELETE,
                entity,
                responseType);


        if (response.getStatusCode().is2xxSuccessful() || response.getStatusCode().is3xxRedirection()) {
            return response;
        }
        log.error("Error response", response);
        throw new RestRepositoryException();
    }*/

    protected UriComponentsBuilder getUriBuilder(String uri) {
        return UriComponentsBuilder.fromHttpUrl(uri);
    }

    protected boolean isOkHttpStatus(int status) {
        return status > 199 && status < 300;
    }

}
